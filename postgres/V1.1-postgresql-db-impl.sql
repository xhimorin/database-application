CREATE TABLE imdb.drop_test (
    id_drop_test   SERIAL      NOT NULL,
    mark	VARCHAR(4)	NOT NULL,
	description VARCHAR(20)	NOT NULL,
    PRIMARY KEY (id_drop_test)
);

ALTER TABLE imdb.drop_test OWNER TO xklamp;

--
-- Name: awards; Type: TABLE; Owner: postgres
--

CREATE TABLE imdb.awards (
    id_awards   SERIAL      NOT NULL,
    title       VARCHAR(16) NOT NULL,
    description VARCHAR(256),
    PRIMARY KEY (id_awards)
);

ALTER TABLE imdb.awards OWNER TO xklamp;


--
-- Name: genders; Type: TABLE; Owner: postgres
--

CREATE TABLE imdb.genders (
    id_genders  SERIAL      NOT NULL,
    gender_type VARCHAR(8) NOT NULL,
    PRIMARY key (id_genders)
);

ALTER TABLE imdb.genders OWNER TO xklamp;


--
-- Name: genres; Type: TABLE; Owner: postgres
--

CREATE TABLE imdb.genres (
    id_genres   SERIAL      NOT NULL,
    genre_type  VARCHAR(16) NOT NULL,
    PRIMARY KEY (id_genres)
);

ALTER TABLE imdb.genres OWNER TO xklamp;


--
-- Name: interests; Type: TABLE; Owner: postgres
--

CREATE TABLE imdb.interests (
    id_interests    SERIAL          NOT NULL,
    interest        VARCHAR(512)    NOT NULL,
    PRIMARY KEY (id_interests)
);

ALTER TABLE imdb.interests OWNER TO xklamp;


--
-- Name: movies; Type: TABLE; Owner: postgres
--

CREATE TABLE imdb.movies (
    id_movies           BIGSERIAL       NOT NULL,
    title               VARCHAR(32)    NOT NULL,
    publication_year    INTEGER         NOT NULL,
    country             VARCHAR(16)     NOT NULL,
    url_trailer         VARCHAR(256),
    contents            VARCHAR(2048),
    PRIMARY KEY (id_movies)
);

ALTER TABLE imdb.movies OWNER TO xklamp;


--
-- Name: persons; Type: TABLE; Owner: postgres
--

CREATE TABLE imdb.persons (
    id_persons  BIGSERIAL   NOT NULL,
    id_genders  INTEGER     NOT NULL,
    first_name  VARCHAR(16) NOT NULL,
    surname     VARCHAR(16) NOT NULL,
    birthdate   date,
    country     VARCHAR(16),
    PRIMARY KEY (id_persons),
    CONSTRAINT fk_persons_genders
        FOREIGN KEY (id_genders) 
        REFERENCES imdb.genders (id_genders)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION
);

ALTER TABLE imdb.persons OWNER TO xklamp;


--
-- Name: persons_type; Type: TABLE; Owner: xklamp
--

CREATE TABLE imdb.persons_type (
    id_persons_type SERIAL      NOT NULL,
    person_type     VARCHAR(8) NOT NULL,
    PRIMARY KEY (id_persons_type)
);

ALTER TABLE imdb.persons_type OWNER TO xklamp;


--
-- Name: movies_has_awards; Type: TABLE; Owner: postgres
--

CREATE TABLE imdb.movies_has_awards (
    id_movies   BIGINT  NOT NULL,
    id_awards   INTEGER NOT NULL,
    award_year  INTEGER NOT NULL,
    CONSTRAINT fk_movies_has_awards_movies 
        FOREIGN KEY (id_movies) 
        REFERENCES imdb.movies (id_movies)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION,
    CONSTRAINT fk_movies_has_awards_awards 
        FOREIGN KEY (id_awards) 
        REFERENCES imdb.awards (id_awards)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION
);

ALTER TABLE imdb.movies_has_awards OWNER TO xklamp;


--
-- Name: movies_has_genres; Type: TABLE; Owner: postgres
--

CREATE TABLE imdb.movies_has_genres (
    id_movies BIGINT    NOT NULL,
    id_genres INTEGER   NOT NULL,
    CONSTRAINT fk_movies_has_genres_movies 
        FOREIGN KEY (id_movies) 
        REFERENCES imdb.movies (id_movies)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION,
    CONSTRAINT fk_movies_has_genres_genres
        FOREIGN KEY (id_genres) 
        REFERENCES imdb.genres (id_genres)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION
);

ALTER TABLE imdb.movies_has_genres OWNER TO xklamp;


--
-- Name: movies_has_interests; Type: TABLE; Owner: postgres
--

CREATE TABLE imdb.movies_has_interests (
    id_movies       BIGINT  NOT NULL,
    id_interests    INTEGER,
    CONSTRAINT fk_movies_has_interests_movies
        FOREIGN KEY (id_movies) 
        REFERENCES imdb.movies (id_movies)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION,
    CONSTRAINT fk_movies_has_interests_interests
        FOREIGN KEY (id_interests) 
        REFERENCES imdb.interests (id_interests)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION
);

ALTER TABLE imdb.movies_has_interests OWNER TO xklamp;


--
-- Name: movies_has_persons; Type: TABLE; Owner: postgres
--

CREATE TABLE imdb.movies_has_persons (
    id_movies       BIGINT  NOT NULL,
    id_persons      BIGINT  NOT NULL,
    id_persons_type INTEGER NOT NULL,
    CONSTRAINT fk_movies_has_persons_movies
        FOREIGN KEY (id_movies) 
        REFERENCES imdb.movies (id_movies)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION,
    CONSTRAINT fk_movies_has_persons_persons
        FOREIGN KEY (id_persons) 
        REFERENCES imdb.persons (id_persons)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION,
    CONSTRAINT fk_movies_has_persons_persons_type
        FOREIGN KEY (id_persons_type) 
        REFERENCES imdb.persons_type (id_persons_type)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION
);

ALTER TABLE imdb.movies_has_persons OWNER TO xklamp;

--
-- Name: roles_type; Type: TABLE; Owner: postgres
--

CREATE TABLE imdb.roles_type (
    id_roles_type   SERIAL        NOT NULL,
    role_type       VARCHAR(16)   NOT NULL,
    PRIMARY KEY (id_roles_type)
);

ALTER TABLE imdb.roles_type OWNER TO xklamp;


--
-- Name: users; Type: TABLE; Owner: postgres
--

CREATE TABLE imdb.users (
    id_users        BIGSERIAL   NOT NULL,
    id_roles_type   INTEGER     NOT NULL,
    id_genders      INTEGER     NOT NULL,
    first_name      VARCHAR(16) NOT NULL,
    surname         VARCHAR(16) NOT NULL,
    email           VARCHAR(32) NOT NULL,
    username        VARCHAR(16) NOT NULL,
    birthdate       DATE        NOT NULL,
    regist_date     TIMESTAMPTZ  NOT NULL,
    PRIMARY KEY (id_users),
    CONSTRAINT fk_users_roles_type 
        FOREIGN KEY (id_roles_type) 
        REFERENCES imdb.roles_type (id_roles_type)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION,
    CONSTRAINT fk_users_genders
        FOREIGN KEY (id_genders) 
        REFERENCES imdb.genders (id_genders)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION
);

ALTER TABLE imdb.users OWNER TO xklamp;


--
-- Name: users_credentials; Type: TABLE; Owner: postgres
--

CREATE TABLE imdb.users_credentials (
    id_users_credentials    BIGSERIAL       NOT NULL,
    id_users                BIGINT          NOT NULL,
    user_password           VARCHAR(512)    NOT NULL,  
    PRIMARY KEY (id_users_credentials),
    CONSTRAINT fk_users_credentials_users 
        FOREIGN KEY (id_users) 
        REFERENCES imdb.users (id_users)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);


ALTER TABLE imdb.users_credentials OWNER TO xklamp;


--
-- Name: log_users_credentials; Type: TABLE; Owner: postgres
--

CREATE TABLE imdb.log_users_credentials (
    id_users         BIGINT          NOT NULL,
    user_password    VARCHAR(512)    NOT NULL, 
    change_date      TIMESTAMPTZ     NOT NULL,    
    CONSTRAINT fk_log_users_credentials_users 
        FOREIGN KEY (id_users) 
        REFERENCES imdb.users (id_users)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);

ALTER TABLE imdb.log_users_credentials OWNER TO xklamp;


--
-- Name: ratings; Type: TABLE; Owner: postgres
--

CREATE TABLE imdb.ratings (
    id_ratings      BIGSERIAL   NOT NULL,
    id_users        BIGINT      NOT NULL,
    id_movies       BIGINT      NOT NULL,
    rating_movie    INTEGER,
    comment_movie   VARCHAR(512),
    PRIMARY KEY (id_ratings),
    CONSTRAINT fk_ratings_users 
        FOREIGN KEY (id_users) 
        REFERENCES imdb.users (id_users)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION,
    CONSTRAINT fk_ratings_movies 
        FOREIGN KEY (id_movies) 
        REFERENCES imdb.movies (id_movies)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION
);

ALTER TABLE imdb.ratings OWNER TO xklamp;

