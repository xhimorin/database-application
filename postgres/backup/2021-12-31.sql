--
-- PostgreSQL database dump
--

-- Dumped from database version 12.9 (Ubuntu 12.9-0ubuntu0.20.04.1)
-- Dumped by pg_dump version 12.9 (Ubuntu 12.9-0ubuntu0.20.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: imdb; Type: SCHEMA; Schema: -; Owner: xklamp
--

CREATE SCHEMA imdb;


ALTER SCHEMA imdb OWNER TO xklamp;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: awards; Type: TABLE; Schema: imdb; Owner: xklamp
--

CREATE TABLE imdb.awards (
    id_awards integer NOT NULL,
    title character varying(16) NOT NULL,
    description character varying(256)
);


ALTER TABLE imdb.awards OWNER TO xklamp;

--
-- Name: awards_id_awards_seq; Type: SEQUENCE; Schema: imdb; Owner: xklamp
--

CREATE SEQUENCE imdb.awards_id_awards_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE imdb.awards_id_awards_seq OWNER TO xklamp;

--
-- Name: awards_id_awards_seq; Type: SEQUENCE OWNED BY; Schema: imdb; Owner: xklamp
--

ALTER SEQUENCE imdb.awards_id_awards_seq OWNED BY imdb.awards.id_awards;


--
-- Name: drop_test; Type: TABLE; Schema: imdb; Owner: xklamp
--

CREATE TABLE imdb.drop_test (
    id_drop_test integer NOT NULL,
    mark character varying(4) NOT NULL,
    description character varying(20) NOT NULL
);


ALTER TABLE imdb.drop_test OWNER TO xklamp;

--
-- Name: drop_test_id_drop_test_seq; Type: SEQUENCE; Schema: imdb; Owner: xklamp
--

CREATE SEQUENCE imdb.drop_test_id_drop_test_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE imdb.drop_test_id_drop_test_seq OWNER TO xklamp;

--
-- Name: drop_test_id_drop_test_seq; Type: SEQUENCE OWNED BY; Schema: imdb; Owner: xklamp
--

ALTER SEQUENCE imdb.drop_test_id_drop_test_seq OWNED BY imdb.drop_test.id_drop_test;


--
-- Name: genders; Type: TABLE; Schema: imdb; Owner: xklamp
--

CREATE TABLE imdb.genders (
    id_genders integer NOT NULL,
    gender_type character varying(8) NOT NULL
);


ALTER TABLE imdb.genders OWNER TO xklamp;

--
-- Name: genders_id_genders_seq; Type: SEQUENCE; Schema: imdb; Owner: xklamp
--

CREATE SEQUENCE imdb.genders_id_genders_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE imdb.genders_id_genders_seq OWNER TO xklamp;

--
-- Name: genders_id_genders_seq; Type: SEQUENCE OWNED BY; Schema: imdb; Owner: xklamp
--

ALTER SEQUENCE imdb.genders_id_genders_seq OWNED BY imdb.genders.id_genders;


--
-- Name: genres; Type: TABLE; Schema: imdb; Owner: xklamp
--

CREATE TABLE imdb.genres (
    id_genres integer NOT NULL,
    genre_type character varying(16) NOT NULL
);


ALTER TABLE imdb.genres OWNER TO xklamp;

--
-- Name: genres_id_genres_seq; Type: SEQUENCE; Schema: imdb; Owner: xklamp
--

CREATE SEQUENCE imdb.genres_id_genres_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE imdb.genres_id_genres_seq OWNER TO xklamp;

--
-- Name: genres_id_genres_seq; Type: SEQUENCE OWNED BY; Schema: imdb; Owner: xklamp
--

ALTER SEQUENCE imdb.genres_id_genres_seq OWNED BY imdb.genres.id_genres;


--
-- Name: interests; Type: TABLE; Schema: imdb; Owner: xklamp
--

CREATE TABLE imdb.interests (
    id_interests integer NOT NULL,
    interest character varying(512) NOT NULL
);


ALTER TABLE imdb.interests OWNER TO xklamp;

--
-- Name: interests_id_interests_seq; Type: SEQUENCE; Schema: imdb; Owner: xklamp
--

CREATE SEQUENCE imdb.interests_id_interests_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE imdb.interests_id_interests_seq OWNER TO xklamp;

--
-- Name: interests_id_interests_seq; Type: SEQUENCE OWNED BY; Schema: imdb; Owner: xklamp
--

ALTER SEQUENCE imdb.interests_id_interests_seq OWNED BY imdb.interests.id_interests;


--
-- Name: log_users_credentials; Type: TABLE; Schema: imdb; Owner: xklamp
--

CREATE TABLE imdb.log_users_credentials (
    id_users bigint NOT NULL,
    user_password character varying(512) NOT NULL,
    change_date timestamp with time zone NOT NULL
);


ALTER TABLE imdb.log_users_credentials OWNER TO xklamp;

--
-- Name: movies; Type: TABLE; Schema: imdb; Owner: xklamp
--

CREATE TABLE imdb.movies (
    id_movies bigint NOT NULL,
    title character varying(32) NOT NULL,
    publication_year integer NOT NULL,
    country character varying(16) NOT NULL,
    url_trailer character varying(256),
    contents character varying(2048)
);


ALTER TABLE imdb.movies OWNER TO xklamp;

--
-- Name: movies_has_awards; Type: TABLE; Schema: imdb; Owner: xklamp
--

CREATE TABLE imdb.movies_has_awards (
    id_movies bigint NOT NULL,
    id_awards integer NOT NULL,
    award_year integer NOT NULL
);


ALTER TABLE imdb.movies_has_awards OWNER TO xklamp;

--
-- Name: movies_has_genres; Type: TABLE; Schema: imdb; Owner: xklamp
--

CREATE TABLE imdb.movies_has_genres (
    id_movies bigint NOT NULL,
    id_genres integer NOT NULL
);


ALTER TABLE imdb.movies_has_genres OWNER TO xklamp;

--
-- Name: movies_has_interests; Type: TABLE; Schema: imdb; Owner: xklamp
--

CREATE TABLE imdb.movies_has_interests (
    id_movies bigint NOT NULL,
    id_interests integer
);


ALTER TABLE imdb.movies_has_interests OWNER TO xklamp;

--
-- Name: movies_has_persons; Type: TABLE; Schema: imdb; Owner: xklamp
--

CREATE TABLE imdb.movies_has_persons (
    id_movies bigint NOT NULL,
    id_persons bigint NOT NULL,
    id_persons_type integer NOT NULL
);


ALTER TABLE imdb.movies_has_persons OWNER TO xklamp;

--
-- Name: movies_id_movies_seq; Type: SEQUENCE; Schema: imdb; Owner: xklamp
--

CREATE SEQUENCE imdb.movies_id_movies_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE imdb.movies_id_movies_seq OWNER TO xklamp;

--
-- Name: movies_id_movies_seq; Type: SEQUENCE OWNED BY; Schema: imdb; Owner: xklamp
--

ALTER SEQUENCE imdb.movies_id_movies_seq OWNED BY imdb.movies.id_movies;


--
-- Name: persons; Type: TABLE; Schema: imdb; Owner: xklamp
--

CREATE TABLE imdb.persons (
    id_persons bigint NOT NULL,
    id_genders integer NOT NULL,
    first_name character varying(16) NOT NULL,
    surname character varying(16) NOT NULL,
    birthdate date,
    country character varying(16)
);


ALTER TABLE imdb.persons OWNER TO xklamp;

--
-- Name: persons_id_persons_seq; Type: SEQUENCE; Schema: imdb; Owner: xklamp
--

CREATE SEQUENCE imdb.persons_id_persons_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE imdb.persons_id_persons_seq OWNER TO xklamp;

--
-- Name: persons_id_persons_seq; Type: SEQUENCE OWNED BY; Schema: imdb; Owner: xklamp
--

ALTER SEQUENCE imdb.persons_id_persons_seq OWNED BY imdb.persons.id_persons;


--
-- Name: persons_type; Type: TABLE; Schema: imdb; Owner: xklamp
--

CREATE TABLE imdb.persons_type (
    id_persons_type integer NOT NULL,
    person_type character varying(8) NOT NULL
);


ALTER TABLE imdb.persons_type OWNER TO xklamp;

--
-- Name: persons_type_id_persons_type_seq; Type: SEQUENCE; Schema: imdb; Owner: xklamp
--

CREATE SEQUENCE imdb.persons_type_id_persons_type_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE imdb.persons_type_id_persons_type_seq OWNER TO xklamp;

--
-- Name: persons_type_id_persons_type_seq; Type: SEQUENCE OWNED BY; Schema: imdb; Owner: xklamp
--

ALTER SEQUENCE imdb.persons_type_id_persons_type_seq OWNED BY imdb.persons_type.id_persons_type;


--
-- Name: ratings; Type: TABLE; Schema: imdb; Owner: xklamp
--

CREATE TABLE imdb.ratings (
    id_ratings bigint NOT NULL,
    id_users bigint NOT NULL,
    id_movies bigint NOT NULL,
    rating_movie integer,
    comment_movie character varying(512)
);


ALTER TABLE imdb.ratings OWNER TO xklamp;

--
-- Name: ratings_id_ratings_seq; Type: SEQUENCE; Schema: imdb; Owner: xklamp
--

CREATE SEQUENCE imdb.ratings_id_ratings_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE imdb.ratings_id_ratings_seq OWNER TO xklamp;

--
-- Name: ratings_id_ratings_seq; Type: SEQUENCE OWNED BY; Schema: imdb; Owner: xklamp
--

ALTER SEQUENCE imdb.ratings_id_ratings_seq OWNED BY imdb.ratings.id_ratings;


--
-- Name: roles_type; Type: TABLE; Schema: imdb; Owner: xklamp
--

CREATE TABLE imdb.roles_type (
    id_roles_type integer NOT NULL,
    role_type character varying(16) NOT NULL
);


ALTER TABLE imdb.roles_type OWNER TO xklamp;

--
-- Name: roles_type_id_roles_type_seq; Type: SEQUENCE; Schema: imdb; Owner: xklamp
--

CREATE SEQUENCE imdb.roles_type_id_roles_type_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE imdb.roles_type_id_roles_type_seq OWNER TO xklamp;

--
-- Name: roles_type_id_roles_type_seq; Type: SEQUENCE OWNED BY; Schema: imdb; Owner: xklamp
--

ALTER SEQUENCE imdb.roles_type_id_roles_type_seq OWNED BY imdb.roles_type.id_roles_type;


--
-- Name: users; Type: TABLE; Schema: imdb; Owner: xklamp
--

CREATE TABLE imdb.users (
    id_users bigint NOT NULL,
    id_roles_type integer NOT NULL,
    id_genders integer NOT NULL,
    first_name character varying(16) NOT NULL,
    surname character varying(16) NOT NULL,
    email character varying(32) NOT NULL,
    username character varying(16) NOT NULL,
    birthdate date NOT NULL,
    regist_date timestamp with time zone NOT NULL
);


ALTER TABLE imdb.users OWNER TO xklamp;

--
-- Name: users_credentials; Type: TABLE; Schema: imdb; Owner: xklamp
--

CREATE TABLE imdb.users_credentials (
    id_users_credentials bigint NOT NULL,
    id_users bigint NOT NULL,
    user_password character varying(512) NOT NULL
);


ALTER TABLE imdb.users_credentials OWNER TO xklamp;

--
-- Name: users_credentials_id_users_credentials_seq; Type: SEQUENCE; Schema: imdb; Owner: xklamp
--

CREATE SEQUENCE imdb.users_credentials_id_users_credentials_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE imdb.users_credentials_id_users_credentials_seq OWNER TO xklamp;

--
-- Name: users_credentials_id_users_credentials_seq; Type: SEQUENCE OWNED BY; Schema: imdb; Owner: xklamp
--

ALTER SEQUENCE imdb.users_credentials_id_users_credentials_seq OWNED BY imdb.users_credentials.id_users_credentials;


--
-- Name: users_id_users_seq; Type: SEQUENCE; Schema: imdb; Owner: xklamp
--

CREATE SEQUENCE imdb.users_id_users_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE imdb.users_id_users_seq OWNER TO xklamp;

--
-- Name: users_id_users_seq; Type: SEQUENCE OWNED BY; Schema: imdb; Owner: xklamp
--

ALTER SEQUENCE imdb.users_id_users_seq OWNED BY imdb.users.id_users;


--
-- Name: awards id_awards; Type: DEFAULT; Schema: imdb; Owner: xklamp
--

ALTER TABLE ONLY imdb.awards ALTER COLUMN id_awards SET DEFAULT nextval('imdb.awards_id_awards_seq'::regclass);


--
-- Name: drop_test id_drop_test; Type: DEFAULT; Schema: imdb; Owner: xklamp
--

ALTER TABLE ONLY imdb.drop_test ALTER COLUMN id_drop_test SET DEFAULT nextval('imdb.drop_test_id_drop_test_seq'::regclass);


--
-- Name: genders id_genders; Type: DEFAULT; Schema: imdb; Owner: xklamp
--

ALTER TABLE ONLY imdb.genders ALTER COLUMN id_genders SET DEFAULT nextval('imdb.genders_id_genders_seq'::regclass);


--
-- Name: genres id_genres; Type: DEFAULT; Schema: imdb; Owner: xklamp
--

ALTER TABLE ONLY imdb.genres ALTER COLUMN id_genres SET DEFAULT nextval('imdb.genres_id_genres_seq'::regclass);


--
-- Name: interests id_interests; Type: DEFAULT; Schema: imdb; Owner: xklamp
--

ALTER TABLE ONLY imdb.interests ALTER COLUMN id_interests SET DEFAULT nextval('imdb.interests_id_interests_seq'::regclass);


--
-- Name: movies id_movies; Type: DEFAULT; Schema: imdb; Owner: xklamp
--

ALTER TABLE ONLY imdb.movies ALTER COLUMN id_movies SET DEFAULT nextval('imdb.movies_id_movies_seq'::regclass);


--
-- Name: persons id_persons; Type: DEFAULT; Schema: imdb; Owner: xklamp
--

ALTER TABLE ONLY imdb.persons ALTER COLUMN id_persons SET DEFAULT nextval('imdb.persons_id_persons_seq'::regclass);


--
-- Name: persons_type id_persons_type; Type: DEFAULT; Schema: imdb; Owner: xklamp
--

ALTER TABLE ONLY imdb.persons_type ALTER COLUMN id_persons_type SET DEFAULT nextval('imdb.persons_type_id_persons_type_seq'::regclass);


--
-- Name: ratings id_ratings; Type: DEFAULT; Schema: imdb; Owner: xklamp
--

ALTER TABLE ONLY imdb.ratings ALTER COLUMN id_ratings SET DEFAULT nextval('imdb.ratings_id_ratings_seq'::regclass);


--
-- Name: roles_type id_roles_type; Type: DEFAULT; Schema: imdb; Owner: xklamp
--

ALTER TABLE ONLY imdb.roles_type ALTER COLUMN id_roles_type SET DEFAULT nextval('imdb.roles_type_id_roles_type_seq'::regclass);


--
-- Name: users id_users; Type: DEFAULT; Schema: imdb; Owner: xklamp
--

ALTER TABLE ONLY imdb.users ALTER COLUMN id_users SET DEFAULT nextval('imdb.users_id_users_seq'::regclass);


--
-- Name: users_credentials id_users_credentials; Type: DEFAULT; Schema: imdb; Owner: xklamp
--

ALTER TABLE ONLY imdb.users_credentials ALTER COLUMN id_users_credentials SET DEFAULT nextval('imdb.users_credentials_id_users_credentials_seq'::regclass);


--
-- Data for Name: awards; Type: TABLE DATA; Schema: imdb; Owner: xklamp
--

COPY imdb.awards (id_awards, title, description) FROM stdin;
1	Oscar	Za najlepší film.
2	Oscar	Za najlepší filmovou hudbu.
3	Oscar	Za najlepší kameru.
4	Oscar	Za najlepší střih zvuku.
5	MTV Movie Award	Za najlepší souboj.
\.


--
-- Data for Name: drop_test; Type: TABLE DATA; Schema: imdb; Owner: xklamp
--

COPY imdb.drop_test (id_drop_test, mark, description) FROM stdin;
1	a	text test text
2	b	hokus pokus
3	c	cilka milka
4	d	dobrota
5	e	emil
\.


--
-- Data for Name: genders; Type: TABLE DATA; Schema: imdb; Owner: xklamp
--

COPY imdb.genders (id_genders, gender_type) FROM stdin;
1	muž
2	žena
3	x-men
\.


--
-- Data for Name: genres; Type: TABLE DATA; Schema: imdb; Owner: xklamp
--

COPY imdb.genres (id_genres, genre_type) FROM stdin;
1	Sci-Fi
2	Akční
3	Dobrodružný
4	Drama
5	Krimi
6	Thriller
7	Komedie
8	Rodinný
9	Romantický
10	Mysteriózní
11	Psychologický
12	Horor
13	Sportovní
14	Životopisný
\.


--
-- Data for Name: interests; Type: TABLE DATA; Schema: imdb; Owner: xklamp
--

COPY imdb.interests (id_interests, interest) FROM stdin;
1	Režisér Denis Villeneuve preferoval při natáčení využívání praktických efektů, proto natáčení probíhalo převážně v exteriérech. „Čelisti se také nenatáčely v bazénu,“ vysvětlil rozhodnutí Villeneuve.
2	Závěrečná scéna byla ve skutečnosti natočena na Panenských ostrovech v Karibiku, avšak ve filmu se má odehrávat na pobřeží Tichého oceánu.
3	Hned při první scéně, kde má hlavní postava (Edward Norton) na parkovišti udeřit Tylera (Brad Pitt) "silně, jak jen může", Norton opravdu trefí Pitta do ucha, ač měl úder jen předstírat. To proto, že si režisér David Fincher před scénou vzal Nortona stranou a řekl mu, ať Pitta udeří doopravdy, aby záběr vypadal zcela reálně. Proto ve výsledné scéně můžeme vidět, jak je Pitt skutečně v bolestech a Norton se jen směje.
\.


--
-- Data for Name: log_users_credentials; Type: TABLE DATA; Schema: imdb; Owner: xklamp
--

COPY imdb.log_users_credentials (id_users, user_password, change_date) FROM stdin;
\.


--
-- Data for Name: movies; Type: TABLE DATA; Schema: imdb; Owner: xklamp
--

COPY imdb.movies (id_movies, title, publication_year, country, url_trailer, contents) FROM stdin;
1	Duna	2021	USA	https://www.youtube.com/watch?v=8g18jFHCLXk	Kultovní sci-fi dílo vypráví o mocenských bojích uvnitř galaktického Impéria, v nichž jde o ovládnutí planety Arrakis: zdroje vzácného koření - melanže, jež poskytuje zvláštní psychické schopnosti, které umožňují cestování vesmírem. Padišáh imperátor svěří správu nad Arrakisem a s ní i komplikovanou těžbu neobyčejné látky vévodovi rodu Atreidů. Celá anabáze je ale součástí spiknutí, z něhož se podaří vyváznout jen vévodovu synovi Paulovi a jeho matce, kteří uprchnou do pouště. Jejich jedinou nadějí jsou nedůvěřiví domorodí obyvatelé fremeni, schopní trvale přežít ve vyprahlé pustině. Mohl by Paul Atreides být spasitelem, na kterého tak dlouho čekají?
2	Vykoupení z věznice Shawshank	1994	USA	https://www.youtube.com/watch?v=iorHgfCOhK8	Mladý bankovní manažer Andy Dufresne (Tim Robbins) je v roce 1947 odsouzen na doživotí za vraždu své ženy a jejího milence, kterou nespáchal. Čeká ho trest v obávané věznici Shawshank. Andy se zde sblíží s černochem Redem (Morgan Freeman), jenž je tu už dvacet let, a během dlouhé doby se jim společně podaří dosáhnout zlepšení zdejších poměrů. Andy se dokonce stane strážcům i nenáviděnému řediteli věznice nepostradatelný jako daňový a finanční poradce.
3	Klub rváčů	1999	USA	https://www.youtube.com/watch?v=MAidmBZD0Bk	Když nemůžete půl roku usnout, celý okolní svět vám začne připadat jako nekonečný sen. Všechno kolem vás je nedokonalou xeroxovou kopií sebe sama. Chodíte do práce, díváte se na televizi a jste vděčni za to, když občas ztratíte vědomí a nevíte o světě. Lidí s podobnými problémy moc není, ale mladý úspěšný úředník, který si říká Jack, je jedním z nich. Má slušnou práci, vydělává slušné peníze, ale trpí nejtěžší formou nespavosti. Na služební cestě se Jack seznámí s Tylerem Durdenem, který mu nabídne příbytek pod podmínkou, že mu vrazí pořádnou ránu. Tato "výměna názorů" se oběma zalíbí a brzy vznikne první Klub rváčů. Místo, kde můžou mladí muži, znechucení světem, odložit své starosti a stát se na pár minut zvířaty.
4	Hon	2012	Dánsko	https://www.youtube.com/watch?v=MxQi3KUXc94	\N
5	Na samotě u lesa	1976	Československo	\N	Úsměvný příběh, ve kterém možná poznáte sami sebe. Pátečním odpolednem vyjíždí Oldřich a Věra Lavičkovi se svými dětmi poprvé na chalupu. Sice žádnou nemají, ale touží po ní a doufají, že přátelé v Loukově jim již nějakou vyhlédli. A opravdu. Jenomže v ní bydlí děda Komárek. Jak to bývalo zvykem, slíbí mu, že ho nechají v chalupě dožít, ale soukromí by přece jenom bylo lepší. Jedině děti přilnou k dědovi, jako by byl jejich vlastní, a jdou tak rodičům příkladem. Když děda onemocní, Lavičkovi si uvědomí, že nemají žádný doklad o tom, že jim chalupa bude po dědově smrti patřit, a bleskově se vypraví i s notářem do Loukova...
6	Nedotknutelní	2011	Francie	https://www.youtube.com/watch?v=d1hTDI4lt0k	Ochrnutý a bohatý aristokrat Philippe si za svého nového opatrovníka vybere Drisse, živelného mladíka z předměstí, kterého právě propustili z vězení. Jinými slovy - najde si na tuto práci tu nejméně vhodnou osobu. Podaří se jim však propojit nemožné: Vivaldiho a populární hudbu, serióznost a žoviální vtípky, luxusní obleky a tepláky. Bláznivým, zábavným, silným, neočekávaným a hlavně „nedotknutelným“, přesně takovým se stane jejich přátelství… Komedie s dramatickou zápletkou o tom, že ani od krku po prsty u nohou nepohyblivý člověk odkázaný na pomoc druhých, nemusí ztratit smysl života. A o tom, že i nejméně pravděpodobné spojení melancholického multimilionáře a extrovertního recidivisty může humorně zapůsobit na diváka a může se z něj stát kasovní trhák.
7	Pulp Fiction:Historky zpodsvětí	1994	USA	https://www.youtube.com/watch?v=zt1uSDXL5NA	Scenárista, režisér a herec Quentin Tarantino (nar. 1963) je bezesporu jedním z nejvýznamnějších tvůrců současného světového filmu. Zaujal už svojí celovečerní prvotinou Gauneři (1991) a svým druhým filmem Pulp Fiction – historky z podsvětí (1994) jen potvrdil své dominantní postavení mezi postmoderními režiséry. Scénář filmu vznikl důmyslným propojením tří povídek spjatých se světem zločinu, jež jsou vyprávěny bez ohledu na časovou posloupnost a nakonec vytvoří uzavřený kruh. Původní název filmu, odkazující k pokleslé krvákové literatuře, dokonale souzní s tím, co se děje na plátně. Výbuchy brutálního násilí jsou odlehčeny velmi černým humorem a jedna bizarní situace za druhou ústí do ještě bizarnějších konců.
8	Gran Torino	2008	Německo	https://www.youtube.com/watch?v=RMhbr2XQblk	\N
9	Zelená míle	1999	USA	https://www.youtube.com/watch?v=Ki4haFrqSrw	Brilantní filmové drama Zelená míle podle knižní předlohy Stephena Kinga natočil režisér Frank Darabont, který se zároveň podílel i na jeho scénáři. Film s vynikajícím Tomem Hanksem v hlavní roli, byl nominován na čtyři Oscary. Centrem příběhu je černý obr John Coffey (Michael Clarke Duncan), čekající na popravu za zločin, který dost možná nespáchal a přesto zemře, a dále pak věčné dilema kolem rozhodování o trestu smrti. Děj filmu se odehrává ve věznici, kde sedí ti největší hříšníci, a kde je hrůzostrašná chodba natřená na zeleno, které všichni říkají Zelená míle. Chodba, na jejímž konci čeká smrt. Pokud se nestane zázrak...
10	Forrest Gump	1994	USA	https://www.youtube.com/watch?v=bLvqoHBptjg	\N
11	Kmotr	1972	USA	\N	Doposud nevinný syn mafiánského bosse je vtažen do krvavého rodinného byznysu, když je jeho otec, Don Vito Corleone, hlava newyorské mafiánské rodiny vážně zraněn. Don Vito Corleone je hlava newyorské mafiánské „rodiny“. Když gangster Sollozzo, který má za zády jinou rodinu mafiánů, oznámí, že chce po celém New Yorku prodávat drogy, přichází problém. Don Vito se důrazně staví proti obchodu s drogami a docela mu stačí hazard, vybírání výpalného a podobné aktivity, kterými si vydělává. Dojde tedy k pokusu o jeho vraždu. Sollozzo pak unese jednoho z poradců Dona Vita a pokusí se přinutit syna Dona Vita, aby s prodejem drog souhlasil, plán ovšem zkrachuje, když Sollozzo zjistí, že je Don Vito stále naživu
12	Mlčení jehňátek	1991	USA	https://www.youtube.com/watch?v=lu-pBQ7kNb4	Mladá citlivá agentka FBI Clarice Steriling je přizvána ke spolupráci do týmu vyšetřujícího sérii vražd. Na pachatele ukazují jen velmi kruté následky jeho činů: Vrah přezdívaný "Buffalo Bill" stahuje vždycky své oběti z kůže a potom je hází do řeky. Clarice má za úkol vypracovat psychologický portrét pachatele. Její nadřízení ji proto vyšlou na "konzultaci" za jiným masovým vrahem, psychiatrem dr. Hannibalem Lecterem, který je vězněn za nejvyšších bezpečnostních opatření. Lecter, který zná ze své praxe vraha i jeho motivy a tuší jeho další postup, drží v rukou všechny nitky a přivádí mladou Clarice na správnou stopu...
13	Sedm	1995	USA	https://www.youtube.com/watch?v=znmZoVkCjpI	\N
14	The Matrix	1999	USA	https://www.youtube.com/watch?v=vKQi3bBA1y8	\N
15	Vetřelec	1979	Velká Británie	\N	Vesmírná loď Nostromo míří zpět na Zemi, astronauti spí. Náhle dojde k jejich automatickému probuzení – nikdo neví, co se děje, protože jsou ještě velmi daleko od cíle cesty. Prostřednictvím centrálního počítače zjistí, že přijímače zachytily signál SOS z blízké planety. Posádka je povinna věc vyšetřit. Vyslaný modul má s přistáním velké problémy. Přesto někteří na průzkum. V jeskynním komplexu objeví podivný organizmus, který vypadá jako kolonie vajíček. 
16	Rivalové	2013	Německo	https://www.youtube.com/watch?v=6k0QyHapx0w	\N
17	Počátek	2010	Velká Británie	https://www.youtube.com/watch?v=6XJ4JQFFYO8	Dom Cobb (Leonardo DiCaprio) je velmi zkušený zloděj a jeho největší mistrovství je v krádeži nejcennějších tajemství. Ovšem není to jen tak obyčejný zloděj. Dom krade myšlenky z lidského podvědomí v době, kdy lidská mysl je nejzranitelnější – když člověk spí. Cobbova nevšední dovednost z něj dělá nejen velmi vyhledávaného experta, ale také ohroženého uprchlíka. Musel obětovat vše, co kdy miloval. Nyní se mu však nabízí šance na vykoupení. Může získat zpět svůj život. Tato poslední zakázka je nejen velmi riskantní, ale zdá se, že i nemožná. Tentokrát nemá za úkol myšlenku ukrást, ale naopak ji zasadit do něčí mysli. Pokud uspěje, bude to dokonalý zločin.
\.


--
-- Data for Name: movies_has_awards; Type: TABLE DATA; Schema: imdb; Owner: xklamp
--

COPY imdb.movies_has_awards (id_movies, id_awards, award_year) FROM stdin;
2	1	1995
2	2	1995
2	3	1995
3	4	2000
3	5	2000
\.


--
-- Data for Name: movies_has_genres; Type: TABLE DATA; Schema: imdb; Owner: xklamp
--

COPY imdb.movies_has_genres (id_movies, id_genres) FROM stdin;
1	1
1	2
1	3
1	4
2	4
2	5
3	4
3	6
4	4
5	7
5	8
5	4
6	7
6	14
6	4
7	4
7	5
8	4
9	4
9	10
9	5
10	7
10	4
10	9
11	4
11	5
12	6
12	5
12	10
12	11
12	4
13	5
13	6
13	12
13	4
14	1
14	2
15	1
15	12
16	2
16	4
16	13
16	14
17	1
17	2
17	3
17	6
17	10
\.


--
-- Data for Name: movies_has_interests; Type: TABLE DATA; Schema: imdb; Owner: xklamp
--

COPY imdb.movies_has_interests (id_movies, id_interests) FROM stdin;
1	1
2	2
3	3
\.


--
-- Data for Name: movies_has_persons; Type: TABLE DATA; Schema: imdb; Owner: xklamp
--

COPY imdb.movies_has_persons (id_movies, id_persons, id_persons_type) FROM stdin;
1	1	1
1	2	2
1	3	2
2	4	1
2	5	2
2	6	2
3	7	1
3	8	2
3	9	2
4	10	1
4	11	2
4	12	2
5	13	1
5	14	2
5	15	2
6	16	1
6	17	2
6	18	2
7	19	1
7	20	2
7	21	2
8	22	1
8	23	2
8	24	2
9	25	1
9	26	2
9	27	2
10	28	1
10	26	2
10	29	2
11	31	1
11	32	2
11	33	2
12	34	1
12	35	2
12	36	2
13	6	1
13	5	2
13	8	2
14	37	1
14	38	2
14	39	2
15	40	1
15	41	2
15	42	2
16	43	2
17	44	1
17	45	2
17	46	2
\.


--
-- Data for Name: persons; Type: TABLE DATA; Schema: imdb; Owner: xklamp
--

COPY imdb.persons (id_persons, id_genders, first_name, surname, birthdate, country) FROM stdin;
1	1	Denis	Villeneuve	1967-10-03	Kanada
2	2	Rebecca	Ferguson	1983-10-19	\N
3	1	Timothée	Chalamet	1995-12-27	\N
4	1	Frank	Darabont	1959-01-28	Francie
5	1	Morgan	Freeman	1937-06-01	USA
6	1	David	Fincher	1962-08-28	USA
7	1	Edward	Norton	1969-08-18	\N
8	1	Brad	Bitt	\N	\N
9	1	Thomas	Vinterberg	1969-05-19	Dánsko
10	1	Mads	Mikkelsen	1965-11-22	Dánsko
11	2	Annika	Wedderkopp	\N	\N
12	1	Jiří	Menzel	1938-02-05	Česko
13	1	Josef	Kemr	1922-06-15	Česko
14	2	Naďa	Urbánková	\N	\N
15	1	Eric	Toledano	1971-07-03	Francie
16	1	Omar	Sy	1978-01-20	Francie
17	2	Joséphine	Meaux	1977-01-23	Francie
18	1	Quentin	Tarantino	1963-03-27	USA
19	1	John	Travolta	1954-02-18	USA
20	1	Samuel	Jackson	1948-12-21	USA
21	1	Clint	Eastwood	1930-05-31	USA
22	2	Dreama	Walker	1986-06-20	USA
23	2	Ahney	Her	1992-07-13	USA
24	1	Tom	Hanks	1956-07-09	USA
25	1	David	Morse	1953-10-11	USA
26	1	Robert	Zemeckis	1951-05-14	USA
27	2	Robin	Wright	1966-04-08	USA
28	1	Francis	Coppola	1939-04-07	USA
29	1	Marlon	Brando	1924-04-03	USA
30	1	Al	Capone	1940-04-25	USA
31	1	Jonathan	Demme	1944-02-22	USA
32	2	Jadie	Foster	1962-11-19	USA
33	1	Anthony 	Hopkins	1937-12-31	USA
34	2	Lilly	Wachowski	1967-12-29	\N
35	1	Keanu	Reeves	1964-09-02	Libanon
36	1	Laurence	Fishburne	1961-07-30	USA
37	1	Ridley	Scott	1937-11-30	Velká Británie
38	2	Sigourney	Weaver	\N	\N
39	1	Tom	Skerritt	1933-08-25	USA
40	2	Veronica	Cartwright	1949-04-20	Velká Británie
41	1	Ron	Howard	1954-03-01	USA
42	1	Daniel	Brühl	1978-06-16	Španělsko
43	1	Chris	Hemsworth	1983-08-11	Austrálie
44	1	Christopher	Nolan	1970-07-30	Velká Británie
45	1	Leonardo	DiCaprio	1974-11-11	USA
46	3	Elliot	Page	1987-02-21	Kanada
47	1	Miloš	Forman	1932-02-18	Československo
48	2	Louise	Fletcher	1934-07-22	USA
49	1	Owen	Wilson	\N	\N
50	1	Jack	Nicholson	1937-04-22	\N
51	2	Veronica	Ružová	1999-07-22	USA
52	2	Veronica	Zarmútená	1979-10-22	Maďarsko
53	1	Jack	Nikolas	1957-04-24	Maďarsko
54	2	Veronica	Nemcová	1978-04-02	USA
55	2	Veronica	Ondrášová	1979-10-22	USA
56	1	Nikolas	Frantovský	1992-04-30	Maďarsko
57	3	Omar	Haluz	1973-04-02	Thajsko
58	1	Nikolas	Klampiarský	1939-10-22	USA
59	1	Nikolas	Markíz	1997-04-30	Maďarsko
\.


--
-- Data for Name: persons_type; Type: TABLE DATA; Schema: imdb; Owner: xklamp
--

COPY imdb.persons_type (id_persons_type, person_type) FROM stdin;
1	Režisér
2	Herec
\.


--
-- Data for Name: ratings; Type: TABLE DATA; Schema: imdb; Owner: xklamp
--

COPY imdb.ratings (id_ratings, id_users, id_movies, rating_movie, comment_movie) FROM stdin;
\.


--
-- Data for Name: roles_type; Type: TABLE DATA; Schema: imdb; Owner: xklamp
--

COPY imdb.roles_type (id_roles_type, role_type) FROM stdin;
1	admin
2	užívateľ
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: imdb; Owner: xklamp
--

COPY imdb.users (id_users, id_roles_type, id_genders, first_name, surname, email, username, birthdate, regist_date) FROM stdin;
2	2	1	admin	admin	admin@admin.cz	admin	2021-12-01	2021-12-31 15:33:52.559889+01
\.


--
-- Data for Name: users_credentials; Type: TABLE DATA; Schema: imdb; Owner: xklamp
--

COPY imdb.users_credentials (id_users_credentials, id_users, user_password) FROM stdin;
2	2	$argon2id$v=19$m=65536,t=3,p=4$F7/IFl9Bu0s1BbZ/cmyfEw$oe2KDx6VFr+k5q1zGEgk54umJ0aHkYrAr8HsIMyZTH8
\.


--
-- Name: awards_id_awards_seq; Type: SEQUENCE SET; Schema: imdb; Owner: xklamp
--

SELECT pg_catalog.setval('imdb.awards_id_awards_seq', 5, true);


--
-- Name: drop_test_id_drop_test_seq; Type: SEQUENCE SET; Schema: imdb; Owner: xklamp
--

SELECT pg_catalog.setval('imdb.drop_test_id_drop_test_seq', 5, true);


--
-- Name: genders_id_genders_seq; Type: SEQUENCE SET; Schema: imdb; Owner: xklamp
--

SELECT pg_catalog.setval('imdb.genders_id_genders_seq', 3, true);


--
-- Name: genres_id_genres_seq; Type: SEQUENCE SET; Schema: imdb; Owner: xklamp
--

SELECT pg_catalog.setval('imdb.genres_id_genres_seq', 14, true);


--
-- Name: interests_id_interests_seq; Type: SEQUENCE SET; Schema: imdb; Owner: xklamp
--

SELECT pg_catalog.setval('imdb.interests_id_interests_seq', 3, true);


--
-- Name: movies_id_movies_seq; Type: SEQUENCE SET; Schema: imdb; Owner: xklamp
--

SELECT pg_catalog.setval('imdb.movies_id_movies_seq', 17, true);


--
-- Name: persons_id_persons_seq; Type: SEQUENCE SET; Schema: imdb; Owner: xklamp
--

SELECT pg_catalog.setval('imdb.persons_id_persons_seq', 59, true);


--
-- Name: persons_type_id_persons_type_seq; Type: SEQUENCE SET; Schema: imdb; Owner: xklamp
--

SELECT pg_catalog.setval('imdb.persons_type_id_persons_type_seq', 2, true);


--
-- Name: ratings_id_ratings_seq; Type: SEQUENCE SET; Schema: imdb; Owner: xklamp
--

SELECT pg_catalog.setval('imdb.ratings_id_ratings_seq', 1, false);


--
-- Name: roles_type_id_roles_type_seq; Type: SEQUENCE SET; Schema: imdb; Owner: xklamp
--

SELECT pg_catalog.setval('imdb.roles_type_id_roles_type_seq', 2, true);


--
-- Name: users_credentials_id_users_credentials_seq; Type: SEQUENCE SET; Schema: imdb; Owner: xklamp
--

SELECT pg_catalog.setval('imdb.users_credentials_id_users_credentials_seq', 2, true);


--
-- Name: users_id_users_seq; Type: SEQUENCE SET; Schema: imdb; Owner: xklamp
--

SELECT pg_catalog.setval('imdb.users_id_users_seq', 2, true);


--
-- Name: awards awards_pkey; Type: CONSTRAINT; Schema: imdb; Owner: xklamp
--

ALTER TABLE ONLY imdb.awards
    ADD CONSTRAINT awards_pkey PRIMARY KEY (id_awards);


--
-- Name: drop_test drop_test_pkey; Type: CONSTRAINT; Schema: imdb; Owner: xklamp
--

ALTER TABLE ONLY imdb.drop_test
    ADD CONSTRAINT drop_test_pkey PRIMARY KEY (id_drop_test);


--
-- Name: genders genders_pkey; Type: CONSTRAINT; Schema: imdb; Owner: xklamp
--

ALTER TABLE ONLY imdb.genders
    ADD CONSTRAINT genders_pkey PRIMARY KEY (id_genders);


--
-- Name: genres genres_pkey; Type: CONSTRAINT; Schema: imdb; Owner: xklamp
--

ALTER TABLE ONLY imdb.genres
    ADD CONSTRAINT genres_pkey PRIMARY KEY (id_genres);


--
-- Name: interests interests_pkey; Type: CONSTRAINT; Schema: imdb; Owner: xklamp
--

ALTER TABLE ONLY imdb.interests
    ADD CONSTRAINT interests_pkey PRIMARY KEY (id_interests);


--
-- Name: movies movies_pkey; Type: CONSTRAINT; Schema: imdb; Owner: xklamp
--

ALTER TABLE ONLY imdb.movies
    ADD CONSTRAINT movies_pkey PRIMARY KEY (id_movies);


--
-- Name: persons persons_pkey; Type: CONSTRAINT; Schema: imdb; Owner: xklamp
--

ALTER TABLE ONLY imdb.persons
    ADD CONSTRAINT persons_pkey PRIMARY KEY (id_persons);


--
-- Name: persons_type persons_type_pkey; Type: CONSTRAINT; Schema: imdb; Owner: xklamp
--

ALTER TABLE ONLY imdb.persons_type
    ADD CONSTRAINT persons_type_pkey PRIMARY KEY (id_persons_type);


--
-- Name: ratings ratings_pkey; Type: CONSTRAINT; Schema: imdb; Owner: xklamp
--

ALTER TABLE ONLY imdb.ratings
    ADD CONSTRAINT ratings_pkey PRIMARY KEY (id_ratings);


--
-- Name: roles_type roles_type_pkey; Type: CONSTRAINT; Schema: imdb; Owner: xklamp
--

ALTER TABLE ONLY imdb.roles_type
    ADD CONSTRAINT roles_type_pkey PRIMARY KEY (id_roles_type);


--
-- Name: users_credentials users_credentials_pkey; Type: CONSTRAINT; Schema: imdb; Owner: xklamp
--

ALTER TABLE ONLY imdb.users_credentials
    ADD CONSTRAINT users_credentials_pkey PRIMARY KEY (id_users_credentials);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: imdb; Owner: xklamp
--

ALTER TABLE ONLY imdb.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id_users);


--
-- Name: log_users_credentials fk_log_users_credentials_users; Type: FK CONSTRAINT; Schema: imdb; Owner: xklamp
--

ALTER TABLE ONLY imdb.log_users_credentials
    ADD CONSTRAINT fk_log_users_credentials_users FOREIGN KEY (id_users) REFERENCES imdb.users(id_users) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: movies_has_awards fk_movies_has_awards_awards; Type: FK CONSTRAINT; Schema: imdb; Owner: xklamp
--

ALTER TABLE ONLY imdb.movies_has_awards
    ADD CONSTRAINT fk_movies_has_awards_awards FOREIGN KEY (id_awards) REFERENCES imdb.awards(id_awards);


--
-- Name: movies_has_awards fk_movies_has_awards_movies; Type: FK CONSTRAINT; Schema: imdb; Owner: xklamp
--

ALTER TABLE ONLY imdb.movies_has_awards
    ADD CONSTRAINT fk_movies_has_awards_movies FOREIGN KEY (id_movies) REFERENCES imdb.movies(id_movies);


--
-- Name: movies_has_genres fk_movies_has_genres_genres; Type: FK CONSTRAINT; Schema: imdb; Owner: xklamp
--

ALTER TABLE ONLY imdb.movies_has_genres
    ADD CONSTRAINT fk_movies_has_genres_genres FOREIGN KEY (id_genres) REFERENCES imdb.genres(id_genres);


--
-- Name: movies_has_genres fk_movies_has_genres_movies; Type: FK CONSTRAINT; Schema: imdb; Owner: xklamp
--

ALTER TABLE ONLY imdb.movies_has_genres
    ADD CONSTRAINT fk_movies_has_genres_movies FOREIGN KEY (id_movies) REFERENCES imdb.movies(id_movies);


--
-- Name: movies_has_interests fk_movies_has_interests_interests; Type: FK CONSTRAINT; Schema: imdb; Owner: xklamp
--

ALTER TABLE ONLY imdb.movies_has_interests
    ADD CONSTRAINT fk_movies_has_interests_interests FOREIGN KEY (id_interests) REFERENCES imdb.interests(id_interests);


--
-- Name: movies_has_interests fk_movies_has_interests_movies; Type: FK CONSTRAINT; Schema: imdb; Owner: xklamp
--

ALTER TABLE ONLY imdb.movies_has_interests
    ADD CONSTRAINT fk_movies_has_interests_movies FOREIGN KEY (id_movies) REFERENCES imdb.movies(id_movies);


--
-- Name: movies_has_persons fk_movies_has_persons_movies; Type: FK CONSTRAINT; Schema: imdb; Owner: xklamp
--

ALTER TABLE ONLY imdb.movies_has_persons
    ADD CONSTRAINT fk_movies_has_persons_movies FOREIGN KEY (id_movies) REFERENCES imdb.movies(id_movies);


--
-- Name: movies_has_persons fk_movies_has_persons_persons; Type: FK CONSTRAINT; Schema: imdb; Owner: xklamp
--

ALTER TABLE ONLY imdb.movies_has_persons
    ADD CONSTRAINT fk_movies_has_persons_persons FOREIGN KEY (id_persons) REFERENCES imdb.persons(id_persons);


--
-- Name: movies_has_persons fk_movies_has_persons_persons_type; Type: FK CONSTRAINT; Schema: imdb; Owner: xklamp
--

ALTER TABLE ONLY imdb.movies_has_persons
    ADD CONSTRAINT fk_movies_has_persons_persons_type FOREIGN KEY (id_persons_type) REFERENCES imdb.persons_type(id_persons_type);


--
-- Name: persons fk_persons_genders; Type: FK CONSTRAINT; Schema: imdb; Owner: xklamp
--

ALTER TABLE ONLY imdb.persons
    ADD CONSTRAINT fk_persons_genders FOREIGN KEY (id_genders) REFERENCES imdb.genders(id_genders);


--
-- Name: ratings fk_ratings_movies; Type: FK CONSTRAINT; Schema: imdb; Owner: xklamp
--

ALTER TABLE ONLY imdb.ratings
    ADD CONSTRAINT fk_ratings_movies FOREIGN KEY (id_movies) REFERENCES imdb.movies(id_movies);


--
-- Name: ratings fk_ratings_users; Type: FK CONSTRAINT; Schema: imdb; Owner: xklamp
--

ALTER TABLE ONLY imdb.ratings
    ADD CONSTRAINT fk_ratings_users FOREIGN KEY (id_users) REFERENCES imdb.users(id_users);


--
-- Name: users_credentials fk_users_credentials_users; Type: FK CONSTRAINT; Schema: imdb; Owner: xklamp
--

ALTER TABLE ONLY imdb.users_credentials
    ADD CONSTRAINT fk_users_credentials_users FOREIGN KEY (id_users) REFERENCES imdb.users(id_users) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: users fk_users_genders; Type: FK CONSTRAINT; Schema: imdb; Owner: xklamp
--

ALTER TABLE ONLY imdb.users
    ADD CONSTRAINT fk_users_genders FOREIGN KEY (id_genders) REFERENCES imdb.genders(id_genders);


--
-- Name: users fk_users_roles_type; Type: FK CONSTRAINT; Schema: imdb; Owner: xklamp
--

ALTER TABLE ONLY imdb.users
    ADD CONSTRAINT fk_users_roles_type FOREIGN KEY (id_roles_type) REFERENCES imdb.roles_type(id_roles_type);


--
-- PostgreSQL database dump complete
--

