from sqlalchemy.orm import backref
from datetime import datetime
from app import db

###
# Model: genders
###
class Genders(db.Model):
    __table_args__ = {'schema': 'imdb'}
    __tablename__ = 'genders'
    id_genders = db.Column(db.Integer, primary_key = True)
    gender_type = db.Column(db.String(8), nullable = False)
    user = db.relationship('Users', backref = 'user_gender') # vztah k Users MANY:ONE

###
# Model: roles_type
###
class RolesType(db.Model):
    __table_args__ = {'schema': 'imdb'}
    __tablename__ = 'roles_type'
    id_roles_type = db.Column(db.Integer, primary_key = True)
    role_type = db.Column(db.String(16), nullable = False)
    #user = db.relationship('Users', backref = 'user_role') # vztah k Users MANY:ONE

###
# Model: users
###
class Users(db.Model):
    __table_args__ = {'schema': 'imdb'}
    __tablename__ = 'users'
    id_users = db.Column(db.Integer, primary_key = True)
    id_roles_type = db.Column(db.Integer, db.ForeignKey('imdb.roles_type.id_roles_type'),
                nullable = False)
    id_genders = db.Column(db.Integer, db.ForeignKey('imdb.genders.id_genders'))
    first_name = db.Column(db.String(16), nullable = False)
    surname = db.Column(db.String(16), nullable = False)
    email = db.Column(db.String(16), nullable = False)
    username = db.Column(db.String(16), nullable = False)
    birthdate = db.Column(db.DateTime, nullable = False)
    regist_date = db.Column(db.DateTime, nullable = False)
    password = db.relationship('UsersCredentials', cascade='all, delete', 
                backref = 'password', uselist = False) # vztah k UsersCredentials ONE:ONE

    def __init__(self, id_roles_type, id_genders, first_name, surname,
            email, username, birthdate):
        self.id_roles_type = id_roles_type
        self.id_genders = id_genders
        self.first_name = first_name
        self.surname = surname
        self.email = email
        self.username = username
        self.birthdate = birthdate
        self.regist_date = datetime.now()
        
    @classmethod
    def create(cls, form):
        new_user = Users(form['role'], form['gender'], 
                form['firstname'], form['surname'], form['email'],
                form['username'], form['birthdate'])
        cls._add_session(new_user)
        new_credential = UsersCredentials(new_user.id_users, form['password'])    
        cls._add_session(new_credential)

    @classmethod
    def _add_session(cls, data):
        db.session.add(data)
        db.session.commit()

    @classmethod
    def update(cls, id, form):
        cls._wrap_update(Users, id, form)
        
    @classmethod
    def _wrap_update(cls, table, id, data):
        table.query.filter_by(id_users=id).update(data)
        db.session.commit()

###
# Model: users_credentials
###
class UsersCredentials(db.Model):
    __table_args__ = {"schema": "imdb"}
    __tablename__ = 'users_credentials'
    id_users_credentials = db.Column(db.Integer, primary_key = True)
    id_users = db.Column(db.Integer, db.ForeignKey('imdb.users.id_users'), nullable = False,
            unique = True)
    user_password = db.Column(db.String(512), nullable = False)

    def __init__(self, id_user, password):
        self.id_users = id_user
        self.user_password = password



###
# Model: drop_test - drop sql injection
###
class DropTest(db.Model):
    __table_args__ = {"schema": "imdb"}
    __tablename__ = 'drop_test'
    id_drop_test = db.Column(db.Integer, primary_key = True)
    mark = db.Column(db.String(4), nullable = False)
    description = db.Column(db.String(20), nullable = False)
