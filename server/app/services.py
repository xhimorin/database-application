from logging import getLevelName

from sqlalchemy import text
import sqlalchemy
from sqlalchemy.sql import select
from app.model import DropTest, Genders, Users, RolesType
from app.utils import HashUtil, LoggerUtil
from app import db


hash_utils = HashUtil()
logger = LoggerUtil(__name__)

#######
# DB QUERY SERVICES
#######
class DBServices:

    # GET password by username
    def get_password_from_db(self, username):
        try:
            user = Users.query.filter_by(username = username).first()
            password = user.password.user_password
            logger.write_log(logger.INFO, f'Login user {username}')
            return password
        except (AttributeError, TypeError) as e:
            logger.write_log(logger.WARNING, f'{self.__class__.__name__} - {e}')
            return False
        except sqlalchemy.exc.NoReferencedTableError as e:
            logger.write_log(logger.ERROR, f'{self.__class__.__name__} - {e}')
            return False

    # GET user information from USERS, ROLE_TYPE and GENDERS
    def get_user_information_from_db(self, username):
        try:
            result = db.session.query(Users, RolesType, Genders) \
                    .join(RolesType, 
                        Users.id_roles_type == RolesType.id_roles_type) \
                    .join(Genders,
                        Users.id_genders == Genders.id_genders) \
                    .filter(Users.username == username).first()
            return result
        except (AttributeError, TypeError) as e:
            logger.write_log(logger.WARNING, f'{self.__class__.__name__} - {e}')
            return False

    # DELETE user by id
    def delete_user_db(self, id):
        try:
            result = Users.query.filter_by(id_users = id).first()
            logger.write_log(logger.INFO, f'Delete user {result.username}')
            db.session.delete(result)
            db.session.commit()
            return True
        except (AttributeError, TypeError) as e:
            logger.write_log(logger.WARNING, f'{self.__class__.__name__} - {e}')
            return False

    # INSERT new user
    def create_user_db(self, form):
        try:
            Users.create(form)
            logger.write_log(logger.INFO, f'Create user {form["username"]}')
            return True
        except (AttributeError, TypeError) as e:
            logger.write_log(logger.WARNING, f'{self.__class__.__name__} - {e}')
            return False

    # GET username
    def get_username_if_exist_db(self, username):
        try:
            result = Users.query.filter_by(username=username).first()
            return False if result == None else True
        except (AttributeError, TypeError) as e:
            logger.write_log(logger.WARNING, f'{self.__class__.__name__} - {e}')
            return False

    # GET role_id by value
    def get_role_id(self, value='užívateľ'):
        try:
            result = RolesType.query.filter_by(role_type=value).first()
            return result.id_roles_type if result != None else False
        except (AttributeError, TypeError) as e:
            logger.write_log(logger.WARNING, f'{self.__class__.__name__} - {e}')
            return False

    # GET id_genders by value
    def get_gender_id(self, value):
        try:
            result = Genders.query.filter_by(gender_type=value).first()
            return result.id_genders if result != None else False
        except (AttributeError, TypeError) as e:
            logger.write_log(logger.WARNING, f'{self.__class__.__name__} - {e}')
            return False

    # UPDATE user data by id
    def update_user_db(self, id_user, user_data):
        try:
            Users.update(id_user, user_data)
            logger.write_log(logger.INFO, f'Update user {user_data.username}')
            return True
        except (AttributeError, TypeError) as e:
            logger.write_log(logger.WARNING, f'{self.__class__.__name__} - {e}')
            return False

    # GET description by id --- DROP table
    def get_drop_test_by_mark_db(self, mark):
        try:
            t = text(f"SELECT imdb.drop_test.description FROM imdb.drop_test WHERE imdb.drop_test.mark = \'{mark}\';")
            result = db.engine.execute(t).first()
            print(result)
            return result[0]
        except Exception as e:
            logger.write_log(logger.WARNING, f'{self.__class__.__name__} - {e}')
            return False

#######
# SERVICE FOR ROUTE FLASK
#######
class Services:

    def __init__(self):
        self.db = DBServices()

    # SERVICE for login
    def login_service(self, username, password):
        db_password = self.db.get_password_from_db(username)
        if db_password:
            return hash_utils.verify_password(db_password, password)
        return False

    # SERVICE for registration
    def registration_service(self, form):
        data = {
                'gender': self.db.get_gender_id(form.json['gender']),
                'role': self.db.get_role_id(),
                'username': form.json['username'],
                'firstname': form.json['firstname'],
                'surname': form.json['surname'],
                'email': form.json['email'],
                'birthdate': form.json['birthdate'],
                'password': hash_utils.get_hash_password(form.json['password']),
            }
        print(data['birthdate'])
        if data['role'] and data['gender']:
            return self.db.create_user_db(data)
        else:
            return False

    # SERVICE for GET user info
    def get_user_info(self, username):
        user, role, gender = self.db.get_user_information_from_db(username)
        if user:
            response_data = {
                'id': user.id_users,
                'gender': gender.gender_type,
                'role': role.role_type,
                'username': username,
                'firstname': user.first_name,
                'surname': user.surname,
                'email': user.email,
                'birthdate': user.birthdate,
            }
            return response_data
        return False

    # SERVICE for DELETE user from DB
    def delete_user_service(self, id):
        db_delete_user = self.db.delete_user_db(id)
        return True if db_delete_user else False

    # SERVICE for GET username
    def get_username_if_exist(self, username):
        return self.db.get_username_if_exist_db(username)

    # SERVICE for UPDATE users data
    def update_user_data(self, data):
        user_data = {
            'id_genders': self.db.get_gender_id(data.json['gender']),
            'id_roles_type': self.db.get_role_id(data.json['role']),
            'username': data.json['username'],
            'first_name': data.json['firstname'],
            'surname': data.json['surname'],
            'email': data.json['email'],
            'birthdate': data.json['birthdate'],
        }
        return self.db.update_user_db(data.json['id'], user_data)

    # SERVICE for get description -- drop table
    def get_description_drop_table(self, data):
        return self.db.get_drop_test_by_mark_db(data.json['mark'])

