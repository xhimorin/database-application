from flask import Flask
from flask_cors import CORS
from flask_jwt_extended.jwt_manager import JWTManager
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SECRET_KEY'] = 't0t08#7sfOP52@78@7'
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://xklamp:xklamp@localhost:5432/imdb'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)

CORS(app, supports_credentials = True, resources = {r'/*': {'origins': 'http://localhost:8080'}})
app.config['JWT_SECRET_KEY'] = 'Super_Secret_JWT_KEY'
app.config['JWT_ACCESS_TOKEN_EXPIRES'] = False
jwt = JWTManager(app)

from app import routes
