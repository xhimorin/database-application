from argon2 import PasswordHasher, exceptions
from datetime import datetime
import logging
import logging.config

class HashUtil:
    def __init__(self):
        self.ph = PasswordHasher()

    def get_hash_password(self, text_passwd):
        return self.ph.hash(text_passwd)

    def verify_password(self, hash_passwd, text_passwd):
        try: 
            return self.ph.verify(hash_passwd, text_passwd)
        except exceptions.VerificationError:
            return False
        except Exception:
            return False


class LoggerUtil:

    INFO = 'INFO'
    WARNING = 'WARNING'
    ERROR = 'ERROR'

    def __init__(self, name):
        self.logger = logging.getLogger(name)
        self.logger.setLevel(logging.INFO)
        self.formatter = logging.Formatter(
            '%(asctime)s - %(name)s - %(levelname)s: %(message)s')
        self.file_name = ''

    # GET path to file and generate file name
    def _get_file_name(self):
        root_dir = '/home/xhimorin/bds/3_projekt/'
        path_dir = root_dir + 'database-application/log/'
        return '{}{:%Y-%m-%d}.log'.format(path_dir, datetime.now())

    # CREATE HANDLER
    def _create_handler(self):
        new_file_name = self._get_file_name()

        if self.file_name is not new_file_name:
            self.file_name = new_file_name
            self._remove_handler()
            self._create_file_handler()
            self._create_stream_handler()

    # CREATE console handler
    def _create_stream_handler(self):
        ch = logging.StreamHandler()
        self._set_handler(ch)

    # CREATE file handler
    def _create_file_handler(self):
        fh = logging.FileHandler(self.file_name)
        fh.setLevel(logging.INFO)
        self._set_handler(fh)

    # SET handler to logger obj
    def _set_handler(self, handler):
        handler.setFormatter(self.formatter)
        self.logger.addHandler(handler)

    # REMOVE exists handler
    def _remove_handler(self):
        for hdlr in self.logger.handlers[:]:
            self.logger.removeHandler(hdlr)
            
    # WRITE LOG
    def write_log(self, log_type, message):
        switcher = {
            'INFO': self.logger.info,
            'WARNING': self.logger.warning,
            'ERROR': self.logger.error
        }
        self._create_handler()
        return switcher[log_type](message)
