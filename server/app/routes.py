from flask import request, jsonify
from flask_jwt_extended import create_access_token
from app import app
from app.services import Services

serv = Services()

@app.route('/')
def test():
    return '<h1>TEST</h1>'

#######
# API POST LOGIN USER BY USERNAME and PASSWORD
#######
@app.route('/login', methods=['GET', 'POST'])
def login():
    response_data = {'success': False}
    username = request.json['username']
    password = request.json['password']

    if not username and not password:
        response_data['msg'] = 'Missing parameters!'
        return jsonify(response_data)

    if serv.login_service(username, password):
        token = create_access_token(identity=username)
        response_data['token'] = token
        response_data['success'] = True
        return jsonify(response_data)
    else:
        response_data['msg'] = 'Wrong credential!'
        return jsonify(response_data)

#######
# API POST CREATE NEW USER
#######
@app.route('/registration', methods=['POST'])
def registration():
    response_data = {'success': False}

    if serv.registration_service(request):
        response_data = {'success': True} 
    return jsonify(response_data)

#######
# API POST USER BY USERNAME
#######
@app.route('/user', methods=['POST'])
def user():
    response_data = {'success': False}
    username = request.json['username']
    user_info = serv.get_user_info(username)

    if user_info:
        response_data['success'] = True
        response_data['data'] = user_info 
    return jsonify(response_data)

#######
# API POST CHECK USERNAME
#######
@app.route('/check-username', methods=['POST'])
def check_username():
    response_data = {'success': False}
    username = request.json['username']
    
    if serv.get_username_if_exist(username):
        response_data['success'] = True
    return jsonify(response_data)

#######
# API DELETE USER 
#######
@app.route('/delete-user/<int:id>', methods=['DELETE'])
def delete_user(id):
    response_data = {'success': False}

    if serv.delete_user_service(id):
        response_data['success'] = True
    return jsonify(response_data)

#######
# API DELETE USER 
#######
@app.route('/update-user', methods=['POST'])
def update_user():
    response_data = {'success': False}

    if serv.update_user_data(request):
        response_data['success'] = True
    return jsonify(response_data)


@app.route('/drop-table', methods=['POST'])
def drop_table():
    response_data = {'success': False}
    data = serv.get_description_drop_table(request)
    if data:
        response_data['msg'] = data
        response_data['success'] = True
    return jsonify(response_data)