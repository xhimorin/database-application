HOSTNAME="localhost"
USERNAME="postgres"
PASSWORD="postgres"
DATABASE="imdb"
BACKUP_DIR="/home/xhimorin/bds/3_projekt/database-application/postgres/backup/"


echo "Pulling Database: This may take a few minutes"
export PGPASSWORD="$PASSWORD"
pg_dump -h $HOSTNAME -U $USERNAME $DATABASE > ${BACKUP_DIR}$(date +%Y-%m-%d).sql
unset PGPASSWORD
echo "Pull Complete"
