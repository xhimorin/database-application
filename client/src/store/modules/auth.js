import axios from "axios";

const state = {
  username: null,
  token: null,
  errorMsg: true,
};

const getters = {
  isAuthenticated: state => !!state.token,
  isErrorMsg: state => state.errorMsg,
  getUsername: state => state.username,
};

const actions = {
  async LogInUser({commit}, authData) {
    await axios.post('/login', authData)
    .then(resp => {
      const success = resp.data.success;
      commit('errorMsg', {success: success});
      if (success === true) {
        commit('authUser', {
        username: authData.username, 
        token: resp.data.token
        });
      }
    }).catch(err => {
      console.log(err);
    })
  },
  LogOutUser({commit}) {
    console.log("Dovidenia");
    commit('clearUser');
  },
  ErrorMsg({commit}, data) {
    commit('errorMsg', {'success': data});
  },
};

const mutations = {
  authUser(state, userData) {
    state.username = userData.username;
    state.token = userData.token;
  },
  clearUser(state) {
    state.username = null;
    state.token = null;
  },
  errorMsg(state, userData) {
    state.errorMsg = userData.success;
  }
};

export default {
  state,
  getters,
  actions,
  mutations,
};
