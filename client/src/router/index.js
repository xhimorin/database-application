import { createRouter, createWebHistory } from "vue-router";
import Home from "@/views/Home.vue";
import Movies from "@/views/Movies.vue";
import Login from "@/views/Login.vue";
import AboutUser from "@/views/AboutUser.vue";
import Registration from "@/views/Registration.vue";
import SQLDrop from "@/views/SQLDrop.vue";

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/movies",
    name: "Movies",
    component: Movies,
  },
  {
    path: "/sql-drop",
    name: "SQLDrop",
    component: SQLDrop,
  },
  {
    path: "/login",
    name: "Login",
    component: Login,
  },
  {
    path: "/user",
    name: "AboutUser",
    component: AboutUser,
  },
  {
    path: "/registration",
    name: "Registration",
    component: Registration,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
