# Database Application

BDS 3. projekt. - cely projekt je v develop rezime

## Zaciname
Pre spustenie projektu je nutne mat 2 terminaly a pgadmin. 

### Backend
Na 1. terminaly bude bezat backend, ktory je napisany za pomoci `microframeworku FLASK` jazyka Python.
Pred samotnym spustenym je potrebne nastavit `virtualenv`
```
$ virtualenv env
$ source env/bin/activate
(env)$ pip3 install -r requirements.txt
```

Po uspesnom nastaveni spustime server:
```
(env)$ python3 server/app.py
```

### Frontend 
Otvorime 2. terminal a zadame nasledovne prikazy na spustenie clienta.
```
$ cd client/
client$ npm run serve
```